require("newrelic");
var express = require("express");
var logfmt = require("logfmt");
var pg = require("pg");
var mailer = require("nodemailer");
var app = express();

app.use(express.bodyParser());

app.use(logfmt.requestLogger());

app.get('/', function(req, res) {
	pg.connect(process.env.HEROKU_POSTGRESQL_GRAY_URL , function(err, client, done) {
		if (!err) {
			client.query('SELECT * FROM issues', function(err, result) {
				res.setHeader('Content-Type', 'application/json');
    			res.end(JSON.stringify(result.rows));
				done();
				if (err) {
					console.log(result.rows);
					return console.error(err);
				};
			});
		} else {
			res.send("error: "+err);
		};
	});
});

app.post('/impactChanged', function(req, res) {
	if (req.body.event === 'verification') {
		res.send();
		return;
	};

	console.log('pre connect');
	pg.connect(process.env.HEROKU_POSTGRESQL_GRAY_URL , function(err, client, done) {
		console.log('Connected with error: ' + err);
		if (!err) {
			var payload = req.body.payload;
			var queryString = 'INSERT INTO issues (issue_id, title, method, impact_level, crashes_count, impacted_devices_count, url, timestamp) VALUES (';
			queryString += payload.display_id + ', ';
			queryString += "'" + payload.title + "'" + ', ';
			queryString += "'" + payload.method + "'" +  ', ';
			queryString += payload.impact_level + ', ';
			queryString += payload.crashes_count + ', ';
			queryString += payload.impacted_devices_count + ', ';
			queryString += "'" + payload.url + "'" + ', ';
			queryString += "DEFAULT";
			queryString += ');';
			client.query(queryString, function(err, result) {
				console.log('queried with error: ' + err + '\n queryString: '+queryString);
				done();
				if (err) {
					res.send(err);
					return console.error(err);
				} else {
					res.send(result);
				};
			});
		} else {
			res.send(err);
		};
	});
});

// send daily email if there are new 
var cronJob = require('cron').CronJob;
new cronJob('00 59 23 * * *', function(){
    
	pg.connect(process.env.HEROKU_POSTGRESQL_GRAY_URL , function(err, client, done) {
		if (!err) {
			client.query('SELECT * FROM issues', function(err, result) {
				//Get today's date
				var todaysDate = new Date();
    			var body = "";
    			result.rows.forEach(function (item) {
    				//Create date from input value
					var inputDate = new Date(item.timestamp);

					//call setHours to take the time out of the comparison
					if(inputDate.setHours(0,0,0,0) == todaysDate.setHours(0,0,0,0)) {
					    //Date equals today's date
					    body += item.title + " - " + item.method + "\n";
					    body += "Issue #" + item.issue_id + "\n";
					    body += "Users affected " + item.impacted_devices_count + "\n",
					    body += "Number of crashes" + item.crashes_count + "\n",
					    body += item.url + "\n";
					    body += "\n";
					};
    			});
    			if (body.length > 0) {
    				var transport = mailer.createTransport();
	    			var mailOptions = {
	    				from: "mobileciservice@ancestry.com",
	    				to: "lparker@ancestry.com",
	    				subject: "New level 2 crashes for " + todaysDate.toDateString(),
	    				text: "" + body
	    			};
	    			transport.sendMail(mailOptions);
    			};
				done();
				if (err) {
					console.log("error in chron job: " + result.rows);
					return console.error(err);
				};
			});
		} else {
			console.log("error in chron job: " + err);
		};
	});
}, null, true, "America/Denver");

var port = process.env.PORT || 5000;
app.listen(port, function() {
	console.log("Listening on " + port);
});
