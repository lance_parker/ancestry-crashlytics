var pg = require('pg').native
  , connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/dailyjs'
  , client
  , query;

console.log(connectionString);
console.log(process.env.DATABASE_URL);

client = new pg.Client(connectionString);
client.connect();

client.query("DROP TABLE issues");

var tableInfo = "issue_id bigserial primary key, ";
tableInfo += "title text, ";
tableInfo += "method text, ";
tableInfo += "impact_level smallint, ";
tableInfo += "crashes_count bigint, ";
tableInfo += "impacted_devices_count bigint, ";
tableInfo += "url text";
query = client.query('CREATE TABLE issues (' + tableInfo + ')');
query.on('end', function() { client.end(); });